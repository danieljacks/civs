﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Civs.Physical;
using Civs.Physical.Cells;
using Civs.Helpers;
using PointPair = System.Tuple<System.Drawing.Point, System.Drawing.Point>;

namespace Civs.UI {
	static class GridDrawer {
		//Fields
		private static Pen OutlinePen = new Pen (Color.Black, GridOutlineWidth);
		private static Pen GridSquarePen = new Pen (Color.DimGray, 1);

		static Size _gridDimensionsClipped = new Size (98, 98); //A bit smaller so edge lines are drawn inside frame
		static Size _gridDimensions = new Size (100, 100);

		//Properties
		public static int GridOutlineWidth { get { return 1; } }

		public static Size GridDimensions {
			get {
				return _gridDimensions;
			}
			set {
				if (_gridDimensions != value) {
					_gridDimensions = value;
					_gridDimensionsClipped = new Size (value.Width - (int) Math.Ceiling (OutlinePen.Width / 2), value.Height - (int) Math.Ceiling (OutlinePen.Width / 2));
				}
			}
		}
		
		private static Size GridDimensionsClipped {
			get {
				return _gridDimensionsClipped;
			}
			set {
				if (_gridDimensionsClipped != value) {
					_gridDimensionsClipped = value;
					_gridDimensions = new Size (value.Width + (int) Math.Ceiling (OutlinePen.Width / 2), value.Height + (int) Math.Ceiling (OutlinePen.Width / 2));
				}
			}
		}

		/*private static void CalculateGridDrawingPoints () {
			CalculateOutlineVert ();
			CalculateOutlineHor ();
			CalculateFillRects ();
		}*/

		/// <summary>
		/// Returns a list of Point,Point tuples, corresponding to the top and bottom of vertical lines
		/// </summary>
		/// <param name="numberOfColumns"></param>
		/// <param name="width"></param>
		/// <returns></returns>
		private static List<PointPair> CalculateOutlineVert (int numberOfColumns, int width, int height) {

			int vertLines = numberOfColumns + 1; //There are columns + 1 vertical lines

			List<PointPair> points = new List<PointPair>();

			for (int x = 0; x < vertLines; x++ ) {
				Point point1 = new Point (MathHelper.ColumnXVal(x, width, numberOfColumns), 0);
				Point point2 = new Point (MathHelper.ColumnXVal(x, width, numberOfColumns), height);
				points.Add (new PointPair (point1, point2));
			}

			return points;
		}

		private static List<PointPair> CalculateOutlineHor (int numberOfRows, int width, int height) {
			int horLines = numberOfRows + 1; //There are columns + 1 horizontal lines

			List<PointPair> points = new List<PointPair>();

			for (int y = 0, i = 0; y < horLines; y++, i++) {
				Point point1 = new Point (0, MathHelper.RowYVal (y, height, numberOfRows));
				Point point2 = new Point (width, MathHelper.RowYVal (y, height, numberOfRows));

				points.Add (new PointPair (point1, point2));
			}

			return points;
		}

		private static List<Rectangle> CalculateFillRects (int numberOfColumns, int numberOfRows, int width, int height) {
			List<Rectangle> rects = new List<Rectangle>();

			//Loop through each colum/row intersection, use that as top-left corner of rect. Last column/row not used - nothing after that to connect to
			for (int y = 0; y < numberOfRows; y++) {
				for (int x = 0; x < numberOfColumns; x++) {
					//Add rectange from top left point to bottom right point
					int xPos = MathHelper.ColumnXVal (x, width, numberOfColumns);
					int yPos = MathHelper.RowYVal (y, height, numberOfRows);
					int xLen = MathHelper.ColumnWidth (x, width, numberOfColumns) - 1; //the -1 gives a white (blank) border around cells...
					int yLen = MathHelper.RowHeight(y, height, numberOfRows) - 1;

					rects.Add( new Rectangle (xPos, yPos, xLen, yLen) );
				}
			}
			return rects;
		}
		
		/// <summary>
		/// Clip dimensions so that all drawn grid cell will be square
		/// </summary>
		/// <param name = "useShortSide">if true, us the shortest length (so everything fits in screen at once), else use longest length (fills screen)</param>
		private static Size ClipDimensions (Size drawDimensions, int numberOfColumns, int numberOfRows, bool useShortSide) {
			int displayWidth = drawDimensions.Width - (int) Math.Ceiling (OutlinePen.Width / 2);
			int displayHeight = drawDimensions.Height - (int) Math.Ceiling (OutlinePen.Width / 2);

			float cellWidth = (float) displayWidth / (float) numberOfColumns;
			float cellHeight = (float) displayHeight / (float) numberOfRows;

			if (useShortSide) {
				if (cellWidth < cellHeight) { //width smaller
					cellHeight = cellWidth; //use width
				} else {
					cellWidth = cellHeight; //use height
				}
			} else { //use long side
				if (cellWidth > cellHeight) { //width longer
					cellHeight = cellWidth; //use length
				} else {
					cellWidth = cellHeight; //use height
				}
			}

			displayWidth = (int) Math.Round (cellWidth * numberOfColumns);
			displayHeight = (int) Math.Round (cellHeight * numberOfRows);

			return new Size (displayWidth, displayHeight);
		}
		
		/// <summary>
		/// Draws a grid to the screen
		/// </summary>
		/// <param name="graphics">Graphics to draw to</param>
		/// <param name="dimensions">Dimensions of the grid to draw (pixels)</param>
		/// <param name="grid">Reference to actual grid to draw</param>
		public static void Draw (Graphics graphics, Grid grid, Size drawDimensions) {
			int columns = grid.Width;
			int rows = grid.Height;

			Size dims = ClipDimensions (drawDimensions, columns, rows, true);

			int xLen = dims.Width;
			int yLen = dims.Height;

			List<PointPair> outlineVertPoints = CalculateOutlineVert (columns, xLen, yLen);
			List<PointPair> outlineHorPoints = CalculateOutlineHor (rows, xLen, yLen);
			List<Rectangle> fillRects = CalculateFillRects (columns, rows, xLen, yLen);

			//Draw rectangles
			DrawFill (graphics, grid, fillRects);

			//Draw outlines
			//DrawVertOutline (graphics, grid, outlineVertPoints);
			//DrawHorOutline (graphics, grid, outlineHorPoints);
		}

		private static void DrawFill (Graphics graphics, Grid grid, List<Rectangle> rects) {
			int columns = grid.Width;
			int rows = grid.Height;

			for (int y = 0, i = 0; y < rows; y++) {
				for (int x = 0; x < columns; x++) {
					int typeID = grid.GetCellType (x, y);
					using ( Brush brush = CellTypes.GetTypeBrush (typeID) ) {
						graphics.FillRectangle (brush, rects[i]);

						//DELETE THIS PLEASE LATER WHEN YOU CAN IMPLEMENT A BETTER SYSTEM THATS NOT HARD CODED
						if (typeID == CellTypes.Creature) {
							graphics.DrawRectangle (OutlinePen, rects[i]);
						}

						i++;
					}
				}
			}
		}

		private static void DrawVertOutline (Graphics graphics, Grid grid, List<PointPair> points) {
			int columns = grid.Width;
			int vertLines = columns + 1;

			for (int i = 0; i < vertLines; i++ ) {
				graphics.DrawLine (OutlinePen, points[i].Item1, points[i].Item2);
			}
		}

		private static void DrawHorOutline (Graphics graphics, Grid grid, List<PointPair> points) {
			int rows = grid.Height;
			int horLines = rows + 1;

			for (int i = 0; i < horLines; i++) {
				graphics.DrawLine (OutlinePen, points[i].Item1, points[i].Item2);
			}
		}

	}
}
