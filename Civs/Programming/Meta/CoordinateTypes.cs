﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civs.Programming.Meta {
	//This is a flag enum. USE POWERS OF 2 AS ENUM VALUES!
	[Flags]
	enum CoordinateTypes {
		Cartesian = 0,
		Radial = 1,
		Polar = 2
	}
}
