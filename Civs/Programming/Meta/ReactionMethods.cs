﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using Civs.Physical;
using Civs.Helpers;

namespace Civs.Programming.Meta {
	/// <summary>
	/// All ReactionMethods return a SimCommand and take CellInfoArgs and ReactionArgs parameters
	/// </summary>
	class ReactionMethods {
		/// <summary>
		/// Generates a SimCommand that moves cell in cellArgs to position specified by reactionArgs
		/// </summary>
		[ReactionMethodAttribute(CoordinateTypes.Cartesian)]
		public static SimCommand Move (CellInfoArgs cellArgs, ReactionArgs reactionArgs) {
			SimCommand command = new SimCommand (SimCommandType.Move, cellArgs.XY, reactionArgs.Int1And2);

			return command;
		}

		public static ReactionMethodDelegate Random () {
			List<MethodInfo> methods = ReflectionHelper.GetStaticMethodsWithAttribute<ReactionMethods, ReactionMethodAttribute> ();

			MethodInfo method = methods[Rnd.State.Next(0, methods.Count)];

			ReactionMethodDelegate dele = (ReactionMethodDelegate) Delegate.CreateDelegate (typeof (StimulusMethodDelegate), method, false);

			if (dele == null) {
				throw new NotImplementedException ("Element of array not of delegate type. Might not be anything in the array i.e no methods that match in StimulusMethods class");
			}

			return dele;
		}
	}
}
