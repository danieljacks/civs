﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Reflection;

using Civs.Physical;
using Civs.Physical.Cells;
using Civs.Helpers;

namespace Civs.Programming.Meta {
	/// <summary>
	/// Contains Stimulus Methods. Each method is a stand-alone package - only return is a bool. 
	/// For now, all methods take CellInfoArgs, containing information about calling cell, and stimArgs, with stimulus-specific arguments.
	/// </summary>
	class StimulusMethods {
		[StimulusMethodAttribute(CoordinateTypes.Cartesian)]
		public static bool isCellAtPos (CellInfoArgs cellArgs, StimulusArgs stimArgs) {
			if (SimBoard.MainGrid.GetCellType (cellArgs.X + stimArgs.Int1, cellArgs.Y + stimArgs.Int2) == stimArgs.Cell.TypeID) {
				return true;
			}
			return false;
		}

		public static StimulusMethodDelegate Random () {
			List<MethodInfo> methods = ReflectionHelper.GetStaticMethodsWithAttribute<StimulusMethods, StimulusMethodAttribute> ();

			MethodInfo method = methods[Rnd.State.Next(0, methods.Count)];

			StimulusMethodDelegate dele = (StimulusMethodDelegate) Delegate.CreateDelegate (typeof (StimulusMethodDelegate), method, false);

			if (dele == null) {
				throw new NotImplementedException ("Element of array not of delegate type. Might not be anything in the array i.e no methods that match in StimulusMethods class");
			}

			return dele;
		}
	}
}
