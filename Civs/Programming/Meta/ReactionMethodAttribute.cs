﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civs.Programming.Meta {
	[AttributeUsage(AttributeTargets.Method)]
	class ReactionMethodAttribute : Attribute {
		/// <summary>
		/// Determines what coortinate types the method can process
		/// </summary>
		public readonly CoordinateTypes coordinateTypes;

		public ReactionMethodAttribute (CoordinateTypes acceptableCoordinateTypes) {
			coordinateTypes = acceptableCoordinateTypes;
		}
	}
}
