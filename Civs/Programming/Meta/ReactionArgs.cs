﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Civs;

namespace Civs.Programming.Meta {
	/// <summary>
	/// Input into all reaction methods. ReactionArgs' position is ALWAYS relative
	/// </summary>
	class ReactionArgs {
		/// <summary>
		/// Check Coordinate property
		/// </summary>
		public int Int1 { get; set; }
		/// <summary>
		/// Check Coordinate property
		/// </summary>
		public int Int2 { get; set; }

		/// <summary>
		/// Cartesian represents (X, Y) values for Int1 and Int2, while Radial represents an area between 2 radii
		/// </summary>
		//public CoordinateType Coordinate { get; set; }

		/// <summary>
		/// Provides access to Int1 and Int2 in Point form
		/// </summary>
		public Point Int1And2 {
			get {
				return new Point (Int1, Int2);
			}
			set {
				Int1 = value.X;
				Int2 = value.Y;
			}
		}

		public ReactionArgs (int int1, int int2) {
			Int1 = int1;
			Int2 = int2;
		}

		/// <summary>
		/// Generates a ReactionArgs with random constrained parameters.
		/// </summary>
		/// <param name="coordinateType">Type of coordinate system to generate for. 
		/// If Cartesian, int1 and int2 will be an (X, Y) coordinate randomly generated within maxDistance.
		/// If Radial, int1 and int2 will represent the area between two radii, with int1 being the inner radius and int2 the outer radius.</param>
		public static ReactionArgs Random () {
			int maxDistance = EvolveData.MaxRadiusDefault;
			//CoordinateType coordinateType = EvolveData.CoordinateProbability.ChooseRandom ();

			if (maxDistance < 1) {
				maxDistance = 1;
			}

			int int1, int2;
			Point randomPoint = Rnd.PointInRadius (new Point (0, 0), maxDistance, false);
			int1 = randomPoint.X;
			int2 = randomPoint.Y;

			ReactionArgs args = new ReactionArgs (int1, int2);

			return args;
		}
	}
}
