﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civs.Programming.Meta {
	//public delegate bool StimulusMethodDelegate (CellInfoArgs cellArgs, StimulusArgs stimArgs);
	delegate bool StimulusMethodDelegate (CellInfoArgs cellArgs, StimulusArgs stimArgs);
}
