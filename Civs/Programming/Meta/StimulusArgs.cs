﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Civs.Physical;
using Civs.Physical.Cells;

namespace Civs.Programming.Meta {
	/// <summary>
	/// Input into all stimulus methods. StimulusArgs' position is ALWAYS relative
	/// </summary>
	class StimulusArgs {
		public CellBase Cell { get; set; }
		/// <summary>
		/// Check Coordinate property
		/// </summary>
		public int Int1 { get; set; }
		/// <summary>
		/// Check Coordinate property
		/// </summary>
		public int Int2 { get; set; }

		/// <summary>
		/// Cartesian represents (X, Y) values for Int1 and Int2, while Radial represents an area between 2 radii
		/// </summary>
		public CoordinateTypes Coordinate { get; set; }

		/// <summary>
		/// Provides access to Int1 and Int2 in Point form
		/// </summary>
		public Point Int1And2 {
			get {
				return new Point (Int1, Int2);
			}
			set {
				Int1 = value.X;
				Int2 = value.Y;
			}
		}

		public StimulusArgs (CellBase cell, int int1, int int2, CoordinateTypes coordinate) {
			Cell = cell;
			Int1 = int1;
			Int2 = int2;
			Coordinate = coordinate;
		}

		/// <summary>
		/// Generates a StimulusArgs with random constrained parameters.
		/// </summary>
		/// <param name="coordinateType">Type of coordinate system to generate for. 
		/// If Cartesian, int1 and int2 will be an (X, Y) coordinate randomly generated within maxDistance.
		/// If Radial, int1 and int2 will represent the area between two radii, with int1 being the inner radius and int2 the outer radius.</param>
		public static StimulusArgs Random () {
			int maxDistance = EvolveData.MaxRadiusDefault;
			CoordinateTypes coordinateType = EvolveData.CoordinateProbability.RandomElement ();

			if (maxDistance < 1) {
				maxDistance = 1;
			}

			int int1, int2;
			if (coordinateType == CoordinateTypes.Cartesian) {
				Point randomPoint = Rnd.PointInRadius (new Point (0, 0), maxDistance, false);
				int1 = randomPoint.X;
				int2 = randomPoint.Y;
			} else { //Radial coordinates
				int1 = Rnd.State.Next (1, maxDistance + 1);
				int2 = Rnd.State.Next (1, int1 + 1);
			}
			
			CellBase randomCell = CellTypes.RandomCell ();

			StimulusArgs args = new StimulusArgs (randomCell, int1, int2, coordinateType);

			return args;
		}
	}
}
