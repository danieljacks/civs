﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civs.Programming.Meta {
	[AttributeUsage(AttributeTargets.Method)]
	class StimulusMethodAttribute : Attribute {
		/// <summary>
		/// Determines what coortinate types the method can process
		/// </summary>
		public readonly CoordinateTypes coordinateTypes;

		public StimulusMethodAttribute (CoordinateTypes acceptableCoordinateTypes) {
			coordinateTypes = acceptableCoordinateTypes;
		}
	}
}
