﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civs.Programming.Meta {
	/// <summary>
	/// Contains probability information regarding intelligences
	/// </summary>
	class EvolveData {
		//Probabilities
		public static Probability<CoordinateTypes> CoordinateProbability;

		//Defaults
		public static int MaxRadiusDefault { get; private set; } = 1;
		public static int MaxBehavioursDefault { get; private set; } = 1;

		static EvolveData () {
			Dictionary<CoordinateTypes, int> coordinates = new Dictionary<CoordinateTypes, int>();
			coordinates.Add (CoordinateTypes.Cartesian, 50);
			coordinates.Add (CoordinateTypes.Radial, 50);


			CoordinateProbability = new Probability<CoordinateTypes> (coordinates);
		}
	}
}
