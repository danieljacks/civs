﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Civs.Programming {
	/// <summary>
	/// Contains per-frame updated information about a cell, which is passed to other functions
	/// </summary>
	class CellInfoArgs {
		public int X { get; set; }
		public int Y { get; set; }

		public Point XY {
			get {
				return new Point (X, Y);
			}
			set {
				X = value.X;
				Y = value.Y;
			}
		}

		public CellInfoArgs (int x, int y) {
			X = x;
			Y = y;
		}
	}
}
