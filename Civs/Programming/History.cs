﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Civs.Physical.Cells;
using Civs.Programming;

namespace Civs.Programming {
	static class History {
		public static List<Creature> LivingCreatures { get; private set; } = new List<Creature> ();

		public static void RegisterCreature (Creature creature) {
			LivingCreatures.Add (creature);
		}

		public static void DeregisterCreature (Creature creature) {
			if (LivingCreatures.Contains (creature)) {
				LivingCreatures.Remove (creature);
			}
		}
	}
}
