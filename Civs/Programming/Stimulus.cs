﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Civs.Programming.Meta;

namespace Civs.Programming {
	class Stimulus {
		private StimulusMethodDelegate Method { get; set; }

		public bool Test (CellInfoArgs cellArgs, StimulusArgs stimArgs) {
			return Method (cellArgs, stimArgs);
		}

		public Stimulus (StimulusMethodDelegate method) {
			Method = method;
		}

		public static Stimulus Random () {
			return new Stimulus (StimulusMethods.Random ());
		}
	}
}
