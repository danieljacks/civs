﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Civs.Programming.Meta;
using Civs.Physical;

namespace Civs.Programming {
	/// <summary>
	/// Contains referenced to Stimulus and Reaction instances, along with the arguments needed to run their contained methods. 
	/// CellInfoArgs must be passed per-tick.
	/// </summary>
	class Behaviour {
		public Stimulus Stimulus { get; set; }
		public Reaction Reaction { get; set; }

		public StimulusArgs StimArgs { get; set; }
		public ReactionArgs ReactArgs { get; set; }

		public Behaviour (Stimulus stimulus, Reaction reaction, StimulusArgs stimulusArgs, ReactionArgs reactionArgs ) {
			Stimulus = stimulus;
			Reaction = reaction;

			StimArgs = stimulusArgs;
			ReactArgs = reactionArgs;
		}

		public bool Test (CellInfoArgs cellArgs) {
			return Stimulus.Test (cellArgs, StimArgs);
		}

		public SimCommand Perform (CellInfoArgs cellArgs) {
			return Reaction.Perform (cellArgs, ReactArgs);
		}

		public static Behaviour Random () {
			return new Behaviour (Stimulus.Random (), Reaction.Random (), StimulusArgs.Random (), ReactionArgs.Random ());
		}
	}
}
