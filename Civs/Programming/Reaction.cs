﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Civs.Physical;
using Civs.Programming.Meta;
using Civs.Programming;

namespace Civs.Programming {
	class Reaction {
		private ReactionMethodDelegate Method { get; set; }

		public SimCommand Perform (CellInfoArgs cellArgs, ReactionArgs reactionArgs) {
			 return Method (cellArgs, reactionArgs);
		}

		public Reaction (ReactionMethodDelegate method) {
			Method = method;
		}

		public static Reaction Random () {
			return new Reaction (ReactionMethods.Random ());
		}
	}
}
