﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civs.Physical;
using Civs.Helpers;
using Civs.Physical.Cells;

/// <summary>
/// Provides high-level functions and settings for the game
/// </summary>
namespace Civs {
	class Game {
		//Default: new RndBoardGenParameters (1, 10, 3, 7);
		public static RndBoardGenParameters GenParams { get; set; } = new RndBoardGenParameters (10, 0, 0, 7);
		public static int GridWidth { get; set; } = 10;
		public static int GridHeight { get; set; } = 10;
		public static int NextSeed { get; set; } = DateTime.Now.Millisecond;
		public static int CurrentSeed { get; set; } = DateTime.Now.Millisecond;

		/// <summary>
		/// Regenerates grid and sets genparams, grid width and height in Game class (for later).
		/// Uses a set seed.
		/// </summary>
		public static void RegenerateGrid (RndBoardGenParameters genParams, int width, int height, int seed) {
			Rnd.Initialise (seed);

			VSHelper.MainForm.labelSeed.Text = seed.ToString();

			SimBoard.MainGrid = GridCreator.RandomGrid (genParams, width, height);

			VSHelper.RedrawPanels ();
		}

		public static void RegenerateGrid (RndBoardGenParameters genParams, int width, int height) {
			RegenerateGrid (genParams, width, height, Rnd.RandomSeed);
		}

		public static void RegenerateGrid (int seed) {
			RegenerateGrid (GenParams, GridWidth, GridHeight, seed);
		}

		public static void RegenerateGrid () {
			RegenerateGrid (GenParams, GridWidth, GridHeight, Rnd.RandomSeed);
		}

		public static void Tick () {
			SimBoard.TickNext ();

			VSHelper.RedrawPanels ();
		}
	}
}
