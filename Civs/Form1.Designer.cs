﻿namespace Civs {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose ();
            }
            base.Dispose (disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent () {
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnRegenerate = new System.Windows.Forms.Button();
			this.inGridWidth = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.inGridHeight = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.inCreatureNumber = new System.Windows.Forms.NumericUpDown();
			this.btnTick = new System.Windows.Forms.Button();
			this.labelSeed = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.btnRegenWithSeed = new System.Windows.Forms.Button();
			this.inSeed = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.inGridWidth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.inGridHeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.inCreatureNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.inSeed)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Location = new System.Drawing.Point(12, 12);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(595, 387);
			this.panel1.TabIndex = 0;
			this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
			// 
			// btnRegenerate
			// 
			this.btnRegenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnRegenerate.Location = new System.Drawing.Point(243, 408);
			this.btnRegenerate.Name = "btnRegenerate";
			this.btnRegenerate.Size = new System.Drawing.Size(230, 39);
			this.btnRegenerate.TabIndex = 1;
			this.btnRegenerate.Text = "Regenerate";
			this.btnRegenerate.UseVisualStyleBackColor = true;
			this.btnRegenerate.Click += new System.EventHandler(this.btnRegenerate_Click);
			// 
			// inGridWidth
			// 
			this.inGridWidth.Location = new System.Drawing.Point(75, 405);
			this.inGridWidth.Name = "inGridWidth";
			this.inGridWidth.Size = new System.Drawing.Size(52, 20);
			this.inGridWidth.TabIndex = 2;
			this.inGridWidth.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.inGridWidth.ValueChanged += new System.EventHandler(this.inGridWidth_ValueChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 407);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(57, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Grid Width";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(9, 434);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(60, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Grid Height";
			// 
			// inGridHeight
			// 
			this.inGridHeight.Location = new System.Drawing.Point(75, 431);
			this.inGridHeight.Name = "inGridHeight";
			this.inGridHeight.Size = new System.Drawing.Size(52, 20);
			this.inGridHeight.TabIndex = 2;
			this.inGridHeight.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.inGridHeight.ValueChanged += new System.EventHandler(this.inGridHeight_ValueChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(133, 407);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(52, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Creatures";
			// 
			// inCreatureNumber
			// 
			this.inCreatureNumber.Location = new System.Drawing.Point(191, 405);
			this.inCreatureNumber.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.inCreatureNumber.Name = "inCreatureNumber";
			this.inCreatureNumber.Size = new System.Drawing.Size(46, 20);
			this.inCreatureNumber.TabIndex = 5;
			this.inCreatureNumber.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.inCreatureNumber.ValueChanged += new System.EventHandler(this.inCreatureNumber_ValueChanged);
			// 
			// btnTick
			// 
			this.btnTick.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
			this.btnTick.Location = new System.Drawing.Point(509, 408);
			this.btnTick.Name = "btnTick";
			this.btnTick.Size = new System.Drawing.Size(98, 39);
			this.btnTick.TabIndex = 6;
			this.btnTick.Text = "Tick";
			this.btnTick.UseVisualStyleBackColor = true;
			this.btnTick.Click += new System.EventHandler(this.btnTick_Click);
			// 
			// labelSeed
			// 
			this.labelSeed.AutoSize = true;
			this.labelSeed.Location = new System.Drawing.Point(165, 434);
			this.labelSeed.Name = "labelSeed";
			this.labelSeed.Size = new System.Drawing.Size(13, 13);
			this.labelSeed.TabIndex = 7;
			this.labelSeed.Text = "0";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(133, 434);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(35, 13);
			this.label4.TabIndex = 8;
			this.label4.Text = "Seed:";
			// 
			// btnRegenWithSeed
			// 
			this.btnRegenWithSeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.btnRegenWithSeed.Location = new System.Drawing.Point(413, 408);
			this.btnRegenWithSeed.Name = "btnRegenWithSeed";
			this.btnRegenWithSeed.Size = new System.Drawing.Size(90, 36);
			this.btnRegenWithSeed.TabIndex = 9;
			this.btnRegenWithSeed.Text = "(With seed)";
			this.btnRegenWithSeed.UseVisualStyleBackColor = true;
			this.btnRegenWithSeed.Click += new System.EventHandler(this.btnRegenWithSeed_Click);
			// 
			// inSeed
			// 
			this.inSeed.Location = new System.Drawing.Point(413, 434);
			this.inSeed.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
			this.inSeed.Name = "inSeed";
			this.inSeed.Size = new System.Drawing.Size(90, 20);
			this.inSeed.TabIndex = 10;
			this.inSeed.ValueChanged += new System.EventHandler(this.inSeed_ValueChanged);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(619, 456);
			this.Controls.Add(this.inSeed);
			this.Controls.Add(this.btnRegenWithSeed);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.labelSeed);
			this.Controls.Add(this.btnRegenerate);
			this.Controls.Add(this.btnTick);
			this.Controls.Add(this.inCreatureNumber);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.inGridHeight);
			this.Controls.Add(this.inGridWidth);
			this.Controls.Add(this.panel1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.inGridWidth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.inGridHeight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.inCreatureNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.inSeed)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

		#endregion

		public System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnRegenerate;
		private System.Windows.Forms.NumericUpDown inGridWidth;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown inGridHeight;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown inCreatureNumber;
		private System.Windows.Forms.Button btnTick;
		private System.Windows.Forms.Label label4;
		public System.Windows.Forms.Label labelSeed;
		private System.Windows.Forms.Button btnRegenWithSeed;
		private System.Windows.Forms.NumericUpDown inSeed;
	}
}

