﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Civs.UI;
using Civs.Physical;
using Civs.Helpers;
using Civs.Physical.Cells;

namespace Civs {
    public partial class Form1 : Form {
        public Form1 () {
            InitializeComponent ();

			VSHelper.MainForm = this; //Now we can reference form when we need it. Hopefully this never changes
			Init.Run ();
		}

		private void Form1_Load (object sender, EventArgs e) {
			inGridWidth.Value = Game.GridWidth;
			inGridHeight.Value = Game.GridHeight;

			inCreatureNumber.Value = Game.GenParams.GetParam (CellTypes.Creature);
		}

		private void panel1_Paint (object sender, PaintEventArgs e) {
			e.Graphics.Clear (Color.AntiqueWhite);

			//Square with shortest panel side as dimensions
			int dim = Math.Min (this.panel1.Width, this.panel1.Height);
			Size dimensions = new Size (dim, dim);

			dimensions = new Size (this.panel1.Width, this.panel1.Height);
			GridDrawer.Draw (e.Graphics, SimBoard.MainGrid, dimensions);
		}

		private void inGridWidth_ValueChanged (object sender, EventArgs e) {
			NumericUpDown widthInput = (NumericUpDown) sender;

			int validatedWidth = Grid.ValidateDimension ((int) widthInput.Value);

			//Shows int for user
			widthInput.Value = validatedWidth;

			//Set Grid width
			Game.GridWidth = validatedWidth;
		}

		private void inGridHeight_ValueChanged (object sender, EventArgs e) {
			NumericUpDown heightInput = (NumericUpDown) sender;

			int validatedHeight = Grid.ValidateDimension(heightInput.Value);

			heightInput.Value = validatedHeight;

			Game.GridHeight = validatedHeight;
		}

		private void btnRegenerate_Click (object sender, EventArgs e) {
			Game.RegenerateGrid ();
		}

		private void inCreatureNumber_ValueChanged (object sender, EventArgs e) {
			NumericUpDown creaturesInput = (NumericUpDown) sender;

			creaturesInput.Maximum = Game.GridWidth * Game.GridHeight;

			Game.GenParams.ChangeParam (CellTypes.Creature, (int) creaturesInput.Value);
		}

		private void btnTick_Click (object sender, EventArgs e) {
			Game.Tick ();
		}

		private void button1_Click (object sender, EventArgs e) {
			int xLen = SimBoard.MainGrid.Width;
			int yLen = SimBoard.MainGrid.Height;

			int x1, x2, y1, y2;
			int cell1Type, cell2Type;
			do {
				x1 = Rnd.State.Next (0, xLen);
				x2 = Rnd.State.Next (0, xLen);

				y1 = Rnd.State.Next (0, yLen);
				y2 = Rnd.State.Next (0, yLen);

				cell1Type = SimBoard.MainGrid.GetCellType (x1, y1);
				cell2Type = SimBoard.MainGrid.GetCellType (x2, y2);
			} while (cell1Type == cell2Type);

			//do {
			//	y1 = Rnd.State.Next (0, yLen);
			//	y2 = Rnd.State.Next (0, yLen);
			//} while (y1 == y2);

			SimBoard.MainGrid.SwapCells (x1, y1, x2, y2);
			VSHelper.RedrawPanels ();
		}

		private void btnRegenWithSeed_Click (object sender, EventArgs e) {
			Game.RegenerateGrid (Game.NextSeed);
		}

		private void inSeed_ValueChanged (object sender, EventArgs e) {
			NumericUpDown numeric = (NumericUpDown) sender;
			Game.NextSeed = (int) numeric.Value;
		}
	}
}
