﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Civs.Helpers;
using csDelaunay;

namespace Civs {
	static class Rnd {
		public static Random State { get; private set; }

		public static int RandomSeed { get { return DateTime.UtcNow.Millisecond; } }

		public static void Initialise (int seed) {
			State = new Random (seed);
		}

		/// <summary>
		/// Sets random state randomly and returns the random seed
		/// </summary>
		/// <returns>random seed used</returns>
		public static int InitialiseRandom () {
			int seed = RandomSeed;
			State = new Random (seed);

			return seed;
		}

		/// <summary>
		/// Generates a quasirandom distribution of points
		/// </summary>
		/// <param name="width">max x coordinate of points plus 1 (0 is a coordinate)</param>
		/// <param name="height">max y coordinate of points plus 1</param>
		/// <param name="numberOfPoints">number of points to distribute</param>
		/// <param name="discrepancy">range 0..1, how 'evenly' distributed points are - 0 is perfectly even, 1 is chaotic</param>
		/// <returns>List of points</returns>
		public static List<Point> QuasirandomDistribution (int width, int height, int numberOfPoints) {
			if (numberOfPoints < 1) {
				return new List<Point> ();
			}

			//MAKE IT SO POINTS CANT OVERLAP

			int xLen = width - 1;
			int yLen = height - 1;

			List<Vector2f> vectors = new List<Vector2f>();

			//Completely random points
			for (int i = 0; i < numberOfPoints; i++) {
				double xPos = State.NextDouble () * xLen;
				double yPos = State.NextDouble () * yLen;

				vectors.Add (new Vector2f (xPos, yPos));
			}

			//Apply Lloyd's algorith for more even point distribution
			Voronoi voronoi = new Voronoi (vectors, new Rectf(0, 0, xLen, yLen), 1);

			//Convert vectors to points
			List<Point> points = new List<Point>();
			foreach (Vector2f v in voronoi.SiteCoords()) {
				points.Add (new Point ((int) Math.Round(v.x), (int) Math.Round(v.y)));
			}

			return points;
		}

		public static Point PointInRadius (int originX, int originY, int radius, bool includeOrigin) {
			//maxDistance++; //Random generating is max exclusive - this should be inclusive

			int minRnd;
			int maxRnd;

			if (radius < 1) {
				radius = 1;
			}

			minRnd = -radius;
			maxRnd = radius + 1; //Random generating is max exclusive - add 1 so this should be inclusive

			int x, y;

			do {
				x = Rnd.State.Next (minRnd, maxRnd);
				y = Rnd.State.Next (minRnd, maxRnd);
			} while ((x == 0 && y == 0) || includeOrigin == true); //Keep choosing values until origin is not included, or origin should be included

			//Should be relative to origin
			x += originX;
			y += originY;

			return new Point (x, y);
		}

		/// <summary>
		/// Generates point within specified radius of the origin
		/// </summary>
		/// <param name="origin">Centre of radius</param>
		/// <param name="radius">maximum distance from origin</param>
		/// <param name="includeOrigin">if it is possible for the origin to be chosen</param>
		/// <returns></returns>
		public static Point PointInRadius (Point origin, int radius, bool includeOrigin) {
			return PointInRadius (origin.X, origin.Y, radius, includeOrigin);
		}

		public static Point PointBetweenRadii (Point origin, int minDist, int maxDist) {
			if (minDist < 0) {
				minDist = 0;
			}

			if (maxDist < minDist) {
				maxDist = minDist;
			}

			List<Point> possiblePoints = new List<Point> ();

			for (int y = -maxDist; y <= maxDist; y++) {
				for (int x = -maxDist; x <= maxDist; x++) {
					if (MathHelper.ChebyshevDistance(0, 0, x, y) >= minDist) {
						possiblePoints.Add (new Point (x, y));
					} 
				}
			}

			if (possiblePoints.Count == 0) {
				return new Point (0, 0);
			}

			int randomIndex = State.Next (0, possiblePoints.Count);
			return possiblePoints[randomIndex];
		}
	}
}
