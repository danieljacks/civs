﻿Implement system where there is a list of 'animated' cells (e.g. creature), then iterate through list rather than whole board - speed tick times
AnimatedCell should be a type derived from CellBase