﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using Civs.Helpers;
using Civs.Physical;
using Civs.Programming.Meta;
using Civs.Programming;

namespace Civs {
	static class Init {
		/// <summary>
		/// Initialises application
		/// </summary>
		public static void Run () {
			//Rnd.Initialise (107);
			Rnd.InitialiseRandom ();

			Game.RegenerateGrid (Game.GenParams, Game.GridWidth, Game.GridHeight);
		}
	}
}
