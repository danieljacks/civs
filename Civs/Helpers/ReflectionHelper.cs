﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Civs.Helpers {
	static class ReflectionHelper {
		/// <summary>
		/// Returns all methods inside called class' type that is static and contains the specified attribute type
		/// </summary>
		public static List<MethodInfo> GetStaticMethodsWithAttribute<TClass, TAttribute> () where TClass : class where TAttribute : Attribute {
			List<MethodInfo> methods = new List<MethodInfo> (typeof(TClass).GetMethods (BindingFlags.Static | BindingFlags.Public));
			List<MethodInfo> methodsWithAttribute = new List<MethodInfo> ();

			//Methods returned from GetMethod already static - no need to check now
			foreach (MethodInfo method in methods) {
				TAttribute attribute = method.GetCustomAttribute<TAttribute>();
				if (attribute != null) {
					methodsWithAttribute.Add (method);
				}
			}

			return methodsWithAttribute;
		}
	}
}
