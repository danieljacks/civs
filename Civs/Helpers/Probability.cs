﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civs.Programming.Meta {
	/// <summary>
	/// Probabilities in separate class for simplicity.
	/// Probabilities ar integers which is the chance out of TotalProbability, or the sum of all elements' probabilities
	/// </summary>
	class Probability<TElement> {
		private Dictionary<TElement, int> _elements = new Dictionary<TElement, int>();
		/// <summary>
		/// Each entry contains the element to choose and the probability of choosing that element (int form)
		/// </summary>
		public Dictionary<TElement, int> Elements {
			get {
				return _elements;
			}
			set {
				_elements = value;
				UpdateTotalProbability ();
			}
		}

		/// <summary>
		/// Each element is chosen with a (Element's Probability) / (TotalProbability) chance,
		/// where TotalProbability is the sum of all elements' probabilities
		/// </summary>
		public int TotalProbability { get; private set; }

		public Probability (Dictionary<TElement, int> elements) {
			Elements = elements;
		}

		/// <summary>
		/// Selects an element randomly based on its probability
		/// </summary>
		/// <returns></returns>
		public TElement RandomElement () {
			return RandomElementFromDictionary (_elements);
		}

		/// <summary>
		/// Selects a random element based on its probability, but only uses elements in the elementsToUse list
		/// </summary>
		public TElement RandomElement (List<TElement> elementsToUse) {
			//Some linq stuff
			Dictionary<TElement, int> elements = elementsToUse	.Where<TElement> (key => elementsToUse.Contains(key))
																.ToDictionary (key => key, key => _elements[key]);
			return RandomElementFromDictionary (elements);
		} //BREAKPOINT HERE TO TEST IF LINQ STUFF WORKS

		//Internal shared logic
		private TElement RandomElementFromDictionary (Dictionary<TElement, int> elements) {
			int rndNum = Rnd.State.Next (0, TotalProbability);
			TElement chosenElement = default (TElement);

			//Algorithms here...
			foreach (KeyValuePair<TElement, int> element in elements) {
				if (rndNum < element.Value) {
					chosenElement = element.Key;
					break;
				} else {
					rndNum -= element.Value;
				}
			}

			return chosenElement;
		}

		private void UpdateTotalProbability () {
			int total = 0;
			foreach (KeyValuePair<TElement, int> entry in _elements) {
				total += entry.Value;
			}
			TotalProbability = total;
		}
	}
}
