﻿using System.Collections.Generic;
using System.Reflection;
using System;
using System.Linq;

namespace Civs.Helpers {
	public static class MathHelper {
		/// <summary>
		/// Gets the x coordinate of the specified column, if total dimensions were divided into Columns number of columns
		/// </summary>
		/// <param name="columnNumber">Column number from left</param>
		/// <returns>X coordinate</returns>
		public static int ColumnXVal (int columnNumber, int width, int numberOfColumns) {
			return (int) Math.Round ( ((double) width / (double) numberOfColumns) * columnNumber);
		}

		/// <summary>
		/// Gets the y coordinate of the specified row, if total dimensions were divided into Rows number of rows
		/// </summary>
		/// <param name="columnNumber">row number from top</param>
		/// <returns>y coordinate</returns>
		public static int RowYVal (int rowNumber, int height, int numberOfRows) {
			return (int) Math.Round (((double) height / (double) numberOfRows) * (double) rowNumber);
		}

		public static int ColumnWidth (int columnNumber, int width, int numberOfColumns) {
			if (columnNumber >= numberOfColumns) {
				return 0;
			}

			int x1 = ColumnXVal (columnNumber, width, numberOfColumns);
			int x2 = ColumnXVal (columnNumber + 1, width, numberOfColumns);

			return Math.Abs (x2 - x1);
		}

		public static int RowHeight (int rowNumber, int height, int numberOfRows) {
			if (rowNumber >= numberOfRows) {
				return 0;
			}

			int y1 = RowYVal (rowNumber, height, numberOfRows);
			int y2 = RowYVal (rowNumber + 1, height, numberOfRows);

			return Math.Abs (y2 - y1);
		}

		///Moves val by delta, wrapping it so that val remains within min_val and max_val
		public static int WrapAround (int val, int delta, int min_val, int max_val) {
			if (delta >= 0) {
				return (val + delta - min_val) % max_val + min_val;
			} else {
				return ((val + delta) + max_val * (-delta) - min_val) % max_val + min_val;
			}
		}

		///Moves val by delta, wrapping it so that val remains within min_val and max_val
		public static float WrapAround (float val, float delta, float min_val, float max_val) {
			if (val + delta >= min_val && val + delta <= max_val) { //Value already in correct range
				return val + delta;
			}

			if (delta >= 0) {
				return (val + delta - min_val) % max_val + min_val;
			} else {
				return ((val + delta) + max_val * (-delta) - min_val) % max_val + min_val;
			}
		}

		///Wraps val so that it remains within min_val and max_val
		public static float WrapAround (float val, float min_val, float max_val) {
			return WrapAround (0, val, min_val, max_val);
		}

		///Wraps val so that it remains within min_val and max_val
		public static int WrapAround (int val, int min_val, int max_val) {
			return WrapAround (0, val, min_val, max_val);
		}

		///Wraps angle so that it remains within 0..359 range
		public static float WrapAngle (float angle) {
			return WrapAround (angle, 0, 359);
		}

		public static T Clamp<T> (T val, T min, T max) where T : IComparable<T> {
			if (val.CompareTo (min) < 0) return min;
			else if (val.CompareTo (max) > 0) return max;
			else return val;
		}

		///Returns quadrant of angle (1..4)
		public static float Quadrant (float angle) {
			angle = WrapAngle (angle);

			float quadrant;
			if (angle >= 0 && angle < 90) {
				quadrant = 1;
			} else if (angle >= 90 && angle < 180) {
				quadrant = 2;
			} else if (angle >= 180 && angle < 270) {
				quadrant = 3;
			} else if (angle >= 270 && angle < 360) {
				quadrant = 4;
			} else {
				quadrant = 0; //ERROR
			}

			return quadrant;
		}

		///Returns -1 if the quadrant would give a negative sin, else returns 1
		public static float SinSign (float quadrant) {
			if (quadrant == 1 || quadrant == 2) {
				return 1;
			} else {
				return -1;
			}
		}

		///Returns -1 if the quadrant would give a negative cos, else returns 1
		public static float CosSign (float quadrant) {
			if (quadrant == 1 || quadrant == 4) {
				return 1;
			} else {
				return -1;
			}
		}

		public static bool FloatSimilar (float a, float b, float margin) {
			if (margin < 0) {
				margin *= -1;
			}

			if (a >= b - margin && a <= b + margin) {
				return true;
			} else {
				return false;
			}
		}

		///Returns the Chebyshev distance between the two vector points
		public static int ChebyshevDistance (int x1, int y1, int x2, int y2) {
			return Math.Max (Math.Abs (x1 - x2), Math.Abs (y1 - y2));
		}

		/*
		///Returns the manhattan distance between current and destination
		public static float ManhattanDistance (Vector2 current, Vector2 destination) {
			return Mathf.Abs (destination.x - current.x) + Mathf.Abs (destination.y - current.y);
		}

		///Returns the Chebyshev distance between the two vector points
		public static float ChebyshevDistance (Vector2 current, Vector2 destination) {
			return Mathf.Max (Mathf.Abs (current.x - destination.x), Mathf.Abs (current.y - destination.y));
		}*/
	}
}