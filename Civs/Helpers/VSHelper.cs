﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Civs.Helpers {
	static class VSHelper {
		public static Form1 MainForm { get; set; }

		/// <summary>
		/// Calls invalidate (in turn calls paint method) for all panel controls in form[0]
		/// </summary>
		public static void RedrawPanels () {
			foreach (Control control in MainForm.Controls) {
				if (control.GetType () == typeof (Panel)) {
					control.Invalidate ();
				}
			}
		}
	}
}
