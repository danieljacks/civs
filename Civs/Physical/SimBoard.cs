﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

using Civs.Physical;
using Civs.UI;
using Civs.Helpers;
using Civs.Physical.Cells;


namespace Civs.Physical {
	/// <summary>
	/// NOTE: Ticks are processed left->right, top->bottom. That means higher cells get move priority (lower cells cant move to square they moved to first). Check if this is exploited
	/// </summary>
	static class SimBoard {
		//_mainGrid used to store real data values - i.e. what's actually on the visible SimulationBoard
		private static Grid _mainGrid = new Grid ();
		//_bufferGrid provides space to make edits without affecting original board - a change (scheduled for next tick) shouldn't change adjacent cell's actions for that same step
		private static Grid _bufferGrid = new Grid ();

		public static Grid MainGrid {
			get {
				return _mainGrid;
			}
			set {
				_mainGrid = value;
			}
		}

		public static Grid BufferGrid {
			get {
				return _bufferGrid;
			}
			private set {
				_bufferGrid = value;
			}
		}

		/// <summary>
		/// Commands are stacked in queue during first pass over board, then commands executed in order of pass. 
		/// This ensures cells do not carry out actions twice (e.g. move down then move again when sweep reaches next row)
		/// </summary>
		public static Queue<SimCommand> CommandsQueue { get; set; } = new Queue<SimCommand> ();

		/// <summary>
		/// Advances to the next tick of the simulation
		/// </summary>
		public static void TickNext () {
			//First pass, where commands are added to queue
			for (int y = 0; y < _mainGrid.Height; y++) {
				for (int x = 0; x < _mainGrid.Width; x++) {
					CellBase cell = MainGrid.GetCell (x, y);

					SimCommand command = cell.Simulate (x, y);
					if (command.Type == SimCommandType.Move) {
						CommandsQueue.Enqueue (command);
					}
				}
			}

			//Second pass, where commands are executed from queue
			while (CommandsQueue.Count != 0) {
				SimCommand command = CommandsQueue.Dequeue ();
				ProcessSimCommand (command, MainGrid);
				SimCommandType ct = command.Type;
			}
		}

		/// <summary>
		/// Applies a SimCommand to a grid - actually changes the grid.  BufferGrid is passed here - changes implemented later
		/// </summary>
		/// <returns>The Grid with the SimCommand applied</returns>
		private static void ProcessSimCommand (SimCommand command, Grid grid) {
			//SimCommand command = ValidateCommand (simCommand);

			switch (command.Type) {
				case SimCommandType.None:
					return;
				case SimCommandType.Move:
					if (grid.GetCellType (command.Target) == CellTypes.Empty) {
						grid.SwapCells (command.Target, command.Origin);
					}

					break;
				default:
					break;

			}
		}

		private static SimCommand ValidateCommand (SimCommand command) {
			if (command.Type == SimCommandType.None) {
				return command;
			}

			Point origin = new Point ( MathHelper.Clamp<int> (command.Origin.X, 0, MainGrid.Width - 1), MathHelper.Clamp<int> (command.Origin.Y, 0, MainGrid.Height - 1));
			Point target = new Point ( MathHelper.Clamp<int> (command.Target.X, 0, MainGrid.Width - 1), MathHelper.Clamp<int> (command.Target.Y, 0, MainGrid.Height - 1));

			return new SimCommand (command.Type, origin, target);
		}

	}
}
