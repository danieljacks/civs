﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

/// <summary>
/// Contains everything that needs to be changed when adding cell types
/// </summary>
namespace Civs.Physical.Cells {
	class CellTypes {
		public const int Uninitialized = 0;
		public const int Empty = 1;
		public const int Creature = 2;
		public const int Wall = 3;
		public const int Food = 4;
		public const int numberOfTypes = 5;



		private static Dictionary<int, string> _typeNames;
		private static Dictionary<int, Brush> _typeBrushes;

		public static Dictionary<int, string> TypeNames {
			get {
				if (_typeNames == null) {
					PopulateTypeTables ();
				}
				return _typeNames;
			}
		}

		public static Dictionary<int, Brush> TypeBrushes {
			get {
				if (_typeBrushes == null) {
					PopulateTypeTables ();
				}
				return _typeBrushes;
			}
		}

		public static int RandomType () {
			return Rnd.State.Next (0, numberOfTypes);
		}

		public static CellBase RandomCell () {
			return CreateCell (RandomType ());
		}

		/// <summary>
		/// Contains definitions for CellTypes
		/// </summary>
		public static void PopulateTypeTables () {
			_typeNames = new Dictionary<int, string> ();
			_typeBrushes = new Dictionary<int, Brush> ();

			TypeNames.Add (Uninitialized, "Uninitialized");
			TypeBrushes.Add (Uninitialized, new SolidBrush (Color.Black));

			TypeNames.Add (Empty, "Empty");
			TypeBrushes.Add (Empty, new SolidBrush (Color.WhiteSmoke));

			TypeNames.Add (Creature, "Creature");
			TypeBrushes.Add (Creature, new SolidBrush (Color.Purple));

			TypeNames.Add (Wall, "Wall");
			TypeBrushes.Add (Wall, new SolidBrush (Color.Goldenrod));

			TypeNames.Add (Food, "Food");
			TypeBrushes.Add (Food, new SolidBrush (Color.LimeGreen));
		}

		/// <summary>
		/// Return a cell derived from CellBase based on type ID - i.e. a factory method for CellBase derived classes
		/// </summary>
		public static CellBase CreateCell (int cellTypeID) {
			switch (cellTypeID) {
				case CellTypes.Uninitialized:
					return new Cells.Uninitialized ();
				case CellTypes.Empty:
					return new Cells.Empty ();
				case CellTypes.Creature:
					return new Cells.Creature ();
				case CellTypes.Wall:
					return new Cells.Wall ();
				case CellTypes.Food:
					return new Cells.Food ();
				default:
					throw new NotImplementedException ("Type not implemented in CreateCell function!");

			}
		}

		public static string GetTypeName (int id) {
			string name;
			if (TypeNames.TryGetValue (id, out name)) {
				return name;
			} else {
				return "NAME_NOT_DEFINED";
			}
		}

		public static Brush GetTypeBrush (int id) {
			Brush solid;
			Brush brush;
			if (TypeBrushes.TryGetValue (id, out brush)) {
				solid = (Brush) brush.Clone (); //Still don't know why this is necessary... Something to do with SolidBrush being a child object of Brush?
				return solid;
			} else {
				return new SolidBrush (Color.Magenta);
			}
		}
	}
}