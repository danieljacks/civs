﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civs.Physical.Cells {
	class Empty : CellBase {
		public override int TypeID {
			get {
				return CellTypes.Empty;
			}
		}
	}
}
