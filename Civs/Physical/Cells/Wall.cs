﻿using System.Collections.Generic;
using Civs.Physical.Cells;
using System.Drawing;

namespace Civs.Physical.Cells {
    class Wall : CellBase {        
        public override int TypeID {
            get {
                return CellTypes.Wall;
            }
        }
    }
}