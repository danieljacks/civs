﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Civs.Programming;

namespace Civs.Physical.Cells {
	class Creature : CellBase {
		public override int TypeID {
			get {
				return CellTypes.Creature;
			}
		}

		public Intelligence Intel { get; set; }

		public Creature () {
			Intel = Intelligence.Random ();

			History.RegisterCreature (this);
		}

		public override SimCommand Simulate (int currentX, int currentY) {
			CellInfoArgs cellArgs = new CellInfoArgs (currentX, currentY);

			return Intel.Tick (cellArgs);

			//return SimCommand.MoveRandomCommand (new Point (currentX, currentY), 1, 1);
		}
	}
}
