﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Civs.Physical.Cells {
    /// <summary>
    /// Base class for cells
    /// </summary>
    abstract class CellBase {
		public abstract int TypeID { get; }
		
		string TypeName {
			get {
				return CellTypes.GetTypeName (TypeID);
			}
		}

        public Brush Brush {
			get {
				return CellTypes.GetTypeBrush (TypeID);
			}
		}

		/// <summary>
		/// Simulate is called every time the SimBoard advances a tick. Override if the cell should be simulated
		/// </summary>
		/// <returns>SimCommand to carry out</returns>
		public virtual SimCommand Simulate (int currentX, int currentY) {
			return new SimCommand (SimCommandType.None, new Point (0, 0), new Point (0, 0));
		}
    }
}
