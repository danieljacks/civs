﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civs.Physical {
	enum GenParamType { Chance, Number}

	/// <summary>
	/// Contains information about generation parameters for a specific cell type. 
	/// All Chance values are relative - 
	/// e.g 146 wall to 292 food to 73 creature is the same as 2:4:1 ratio
	/// All number values are absolute - 
	/// e.g. 6 creatures is 6 exact creatures
	/// </summary>
	class GenParam {
		/// <summary>
		/// Type of cell this parameter refers to
		/// </summary>
		public int TypeID { get; private set; }

		/// <summary>
		/// Relative chance to spawn or number of objects to spawn accross board
		/// </summary>
		public int Value { get; set; }

		/// <summary>
		/// Whether Value refers to a chance or a number
		/// </summary>
		public GenParamType Type { get; private set; }

		public GenParam (int typeID, int value, GenParamType type) {
			this.TypeID = typeID;
			this.Value = value;
			this.Type = type;
		}
	}
}
