﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Civs.Physical {
	enum SimCommandType {
		None,
		Move,
		Destroy,
		Create
	}

	/// <summary>
	/// Tells the grid to do something
	/// </summary>
	class SimCommand {
		/// <summary>
		/// What the command will do
		/// </summary>
		public SimCommandType Type { get; private set; }

		/// <summary>
		/// Which cell the command originated froms
		/// </summary>
		public Point Origin { get; set; }

		/// <summary>
		/// What cell the command will act on
		/// </summary>
		public Point Target { get; private set; }

		/*public static SimCommand MoveRandomCommand (Point origin, int minDistance, int maxDistance) {
			maxDistance++; //Random generating is max exclusive - this should be inclusive

			int minRnd;
			int maxRnd;

			if (minDistance < 1) {
				minDistance = 1;
			}

			minRnd = -minDistance;
			maxRnd = maxDistance;

			int x, y;

			do {
				x = Rnd.State.Next (minRnd, maxRnd);
				y = Rnd.State.Next (minRnd, maxRnd);
			} while (x == 0 && y == 0);

			//Should be relative to origin
			x += origin.X;
			y += origin.Y;

			return new SimCommand (SimCommandType.Move, origin, new Point (x, y));
		}*/

		public SimCommand (SimCommandType type, Point origin, Point target) {
			Type = type;
			Origin = origin;
			Target = target;
		}

		public static SimCommand Default () {
			return new SimCommand (SimCommandType.Move, new Point (0, 0), new Point (0, 0));
		}
	}
}
