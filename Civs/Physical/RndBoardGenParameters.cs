﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civs.Physical.Cells;

namespace Civs.Physical {
	class RndBoardGenParameters {
		public List<GenParam> GenParams = new List<GenParam>();

		public int ChanceTotal { get; private set; }

		public RndBoardGenParameters (int emptyChance, int wallChance, int foodChance, int creaturesNumber) {
			GenParams.Add (new GenParam (CellTypes.Empty, emptyChance, GenParamType.Chance));
			GenParams.Add (new GenParam (CellTypes.Wall, wallChance, GenParamType.Chance));
			GenParams.Add (new GenParam (CellTypes.Food, foodChance, GenParamType.Chance));
			GenParams.Add (new GenParam (CellTypes.Creature, creaturesNumber, GenParamType.Number));

			SetChanceTotal ();
		}

		/// <summary>
		/// Changes genParam with indicated TypeID, setting its value (chance/number etc..) to newValue
		/// If genParam is not found, nothing happens
		/// </summary>
		public void ChangeParam (int typeID, int newValue) {
			foreach (GenParam p in GenParams) {
				if (p.TypeID == typeID) {
					p.Value = newValue;
				}
			}
		}

		public int GetParam (int typeID) {
			foreach (GenParam p in GenParams) {
				if (p.TypeID == typeID) {
					return p.Value;
				}
			}

			return -1;
		}

		/// <summary>
		/// Set chance total to the sum of all chances from gen params
		/// </summary>
		private void SetChanceTotal () {
			int total = 0;
			foreach (GenParam param in GenParams) {
				if (param.Type == GenParamType.Chance) {
					total += param.Value;
				}
			}
			ChanceTotal = total;
		}
	}
}
