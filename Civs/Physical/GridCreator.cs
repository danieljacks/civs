﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civs.Physical;
using Civs;
using Civs.Physical.Cells;
using System.Drawing;

/// <summary>
/// Creates grid scenarios
/// </summary>
namespace Civs.Physical {
	static class GridCreator {

		public static Grid RandomGrid (RndBoardGenParameters genParams, int width, int height) {
			Grid grid = new Grid (width, height);

			//Deal with number params first (they might overwrite each other...)
			foreach (GenParam genParam in genParams.GenParams) {
				if (genParam.Type == GenParamType.Number) {
					grid = DistributeNumberParam (grid, genParam);
				}
			}
		
			//Fill in rest with chance params
			for (int y = 0; y < grid.Height; y++) {
				for (int x = 0; x < grid.Width; x++) {
					if (grid.GetCellType (x, y) == CellTypes.Uninitialized) { //Dont overwrite any cells - those were placed by number genParam distributor
						int id = RandomCellID (genParams);
						grid.CreateCell (id, x, y);
					}
				}
			}

			return grid;
		}

		/// <summary>
		/// Distributes cells in a quasirandom manner based on a genParam (overwrites any cells under it!)
		/// </summary>
		/// <param name="grid">input grid to modify</param>
		/// <param name="genParam">details about cell to distribute</param>
		/// <returns>modified grid with new cells distributed</returns>
		private static Grid DistributeNumberParam (Grid grid, GenParam genParam) {
			List<Point> points = Rnd.QuasirandomDistribution (grid.Width, grid.Height, genParam.Value);

			foreach (Point point in points) {
				grid.CreateCell (genParam.TypeID, point.X, point.Y);
			}

			return grid;
		}

		/// <summary>
		/// Generates a random cell type ID based on input generation parameters
		/// </summary>
		/// <param name="genParams">parameters to use</param>
		/// <returns>ID of a cell type</returns>
		private static int RandomCellID (RndBoardGenParameters genParams) {
			int rndNum = Rnd.State.Next (0, genParams.ChanceTotal);
			int CellType = 0;

			//Algorithms here...
			for (int i = 0; i < genParams.GenParams.Count; i++) {
				if (rndNum < genParams.GenParams[i].Value) {
					CellType = genParams.GenParams[i].TypeID;
					break;
				} else {
					rndNum -= genParams.GenParams[i].Value;
				}
			}

			return CellType;
		}
	}
}
