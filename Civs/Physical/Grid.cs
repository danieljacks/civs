﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civs.Physical.Cells;
using Civs.Helpers;
using System.Windows;
using System.Drawing;

//Floor is implicitly implied. One layer of cell objects
//Grid used as SimBoard - both main and buffer grids have same dimensions
namespace Civs.Physical {
	class Grid {
		//15, 10
		public int Width { get; private set; }
		public int Height { get; private set; }

		private CellBase[,] _data;
		private CellBase[,] Data {
			get {
				if (_data == null) {
					_data = PopulateGrid (CellTypes.Uninitialized);
				}
				return _data;
			}
			set {
				_data = value;
			}
		}
		
		/// <summary>
		/// Initialises a Grid with all values set to 0 (empty)
		/// </summary>
		public Grid (int width, int height) {
			Width = width;
			Height = height;
		}

		/// <summary>
		/// Returns a validated dimension i.e. greater than 1 and an int
		/// </summary>
		public static int ValidateDimension (int dimension) {
			return Math.Max (1, dimension);
		}

		public static int ValidateDimension (decimal dimension) {
			return (int) Math.Max (1, dimension);
		}

		/// <summary>
		/// Initializes empty grid of default size 15,10 (Basically a placeholder grid - should ususally be replaced with another grid)
		/// </summary>
		public Grid () : this(15, 10) { }

		/// <summary>
		/// Fills grid with a single type of cell, determined by type id
		/// </summary>
		/// <param name="cellTypeID"></param>
		private CellBase[,] PopulateGrid (int cellTypeID) {
			CellBase[,] grid = new CellBase[Width, Height];

			for (int y = 0; y < Height; y++) {
				for (int x = 0; x < Width; x++) {
					grid[x, y] = CellTypes.CreateCell (cellTypeID);
				}
			}

			return grid;
		}

        /// <summary>
        /// Return instance of the CellBase at specified position
        /// </summary>
        /// <param name="x">X-coordinate of cell</param>
        /// <param name="y">Y-Coordiante of cell</param>
        /// <returns>Instance of CellBase</returns>
		public CellBase GetCell (int x, int y) {
			if (new Rectangle (0,0, Width, Height).Contains (x, y) == false) {
				return new Uninitialized ();
			}

			CellBase cell = Data[x, y];
			return cell;
		}

		public CellBase GetCell (Point xy) {
			return GetCell (xy.X, xy.Y);
		}

        /// <summary>
        /// Return Type ID of the CellBase at specified position
        /// </summary>
        /// <param name="x">X-coordinate of cell</param>
        /// <param name="y">Y-Coordiante of cell</param>
        /// <returns>Type ID of CellBase</returns>
        public int GetCellType (int x, int y) {
            return GetCell (x, y).TypeID;
        }

		public int GetCellType (Point xy) {
			return GetCellType (xy.X, xy.Y);
		}

		public void SetCell (CellBase cell, int x, int y) {
			if (new Rectangle (0, 0, Width, Height).Contains (x, y) == false) {
				return;
			}

			Data[x, y] = cell;
		}

		public void SetCell (CellBase cell, Point xy) {
			SetCell (cell, xy.X, xy.Y);
		}

		/// <summary>
		/// Set cell from cell id
		/// </summary>
		public void CreateCell (int cellTypeID, int x, int y) {
			SetCell (CellTypes.CreateCell (cellTypeID), x, y);
		}

		public void SwapCells (int x1, int y1, int x2, int y2) {
			CellBase cell1 = GetCell (x1, y1);
			CellBase cell2 = GetCell (x2, y2);

			SetCell (cell1, x2, y2);
			SetCell (cell2, x1, y1);
		}

		public void SwapCells (Point cell1, Point cell2) {
			SwapCells (cell1.X, cell1.Y, cell2.X, cell2.Y);
		}

	}
}
